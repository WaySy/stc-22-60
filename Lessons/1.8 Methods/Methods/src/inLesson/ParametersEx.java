package inLesson;

import java.util.Arrays;

public class ParametersEx {
    public static void main(String[] args) {
//        int number = 10;
//        number = multiple(number);
//        System.out.println(number);
        int[] array = {0, 1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(array));
        multipleElementsInArray(array);
        System.out.println(Arrays.toString(array));
        //\u____ char = \u1234 - F
        //\u0000 char = \u1235 - E
    }

    //В Java в метод переменные ВСЕГДА передаются по ЗНАЧЕНИЮ
    //number = 10;
    //multiple(number); копируем значение переменной number и отдаем это значение методу.
    //т.е. внутри метода с этим значением может происходить все что угодно, оригинал (от кого скопировали значение) - не изменится
//    public static void multiple(int value) {
//        value = value * value;
//    }
    public static int multiple(int value) {
        value = value * value;
        return value;
    }

    //Есили в метод, в качестве параметра, передается ссылочная переменная, то в метод так же копируется значение
    //данной переменной, но так как она ссылочная, то она хранитя внутри себя ссылку на объект. Т.е. в метод
    //копируется ссылка на объект и в методе мы работаем с оригиналом (с объектом)
    public static void multipleElementsInArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * 2;
        }
    }
}
