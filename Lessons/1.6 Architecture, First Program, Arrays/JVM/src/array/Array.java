package array;

public class Array {
    public static void main(String[] args) {
        int oleg = 26;
        int vasya = 62;
        int iezekel = 13;

        oleg = oleg - 1;
        vasya = vasya - 1;
        iezekel = iezekel - 1;

        //Способы объявления массива
        //1
        int[] array1 = new int[5]; //[0],[0],[0],[0],[0]
        //2
        int array2[] = new int[5]; //[0],[0],[0],[0],[0]
        //3
        int[] array3 = {1, 11, 989, 675, 43}; //[1],[11],[989],[675],[43]

        // 0   1   2   3   4
        //[0],[0],[0],[0],[0]

        // 0    1    2     3     4
        //[1],[11],[989],[675],[43]
        //ИНИЦИАЛИЗАЦИЯ БЕГУНКА; УСЛОВИЕ РАБОТЫ ЦИКЛА; ЧТО ДЕЛАТЬ С БЕГУНКОМ, ПОСЛЕ ИТЕРАЦИИ)
        //МАССИВ.length - возвращает длину массива
        for (int i = 0; i < array3.length; i = i + 1) {
            //ЕСЛИ УСЛОВИЕ В СКОБКАХ ИСТИНА, ТО ЗАХОДИМ ВНУТРЬ СКОБОК
            if (array3[i] == 989) {
                System.out.println("Нашли");
                break; //прерывает работу цикла
            }
        }

        System.out.println(array3[3]);
    }
}
